package site.zhanjingbo.service;

import org.springframework.stereotype.Component;

@Component
public class ProductService {
	
	public void add(String product){
		System.out.println("添加商品:"+product);
	}
	public void edit(String product){
		System.out.println("修改商品:"+product);
	}
	public void del(String product){
		System.out.println("删除商品:"+product);
	}
	public void find(String product){
		System.out.println("查找商品:"+product);
	}
}
